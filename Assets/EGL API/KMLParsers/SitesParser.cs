﻿using EGL.DataModel;
using EGL.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
namespace EGL.KMLParser
{
    public class SitesParser : KMLFolderParser<Site>,IEGLEventAnnouncer
    {
        public List<Site> siteList;
        public SitesParser()
        {
            siteList = new List<Site>();
        }

        public void announce()
        {
            ParsingSiteCompletionEvent siteEvent = new ParsingSiteCompletionEvent(ref Han.currentSequence, this, siteList);
            EventHub.announceParsingSiteCompletion(siteEvent);
        }

        public override IEnumerator ParseLogic()
        {
            XmlNodeList sitesNodeList = folder.SelectNodes(".//Placemark");
            foreach (XmlNode siteNode in sitesNodeList)
            {
                Site site = new DataModel.Site();
                site.name = siteNode["name"].InnerText;
                site.description = siteNode["description"].InnerText;

                float latitude = Convert.ToSingle(siteNode["LookAt"]["latitude"].InnerText);
                float longitude = Convert.ToSingle(siteNode["LookAt"]["longitude"].InnerText);
                float altitude = float.Parse(siteNode["LookAt"]["altitude"].InnerText);
                Coordinates coordinates = new Coordinates(latitude,longitude,altitude);
                site.placemark.coordinatesList.Add(coordinates);
                site.placemark.SetAltitudeMode("relativeToGround");
                siteList.Add(site);
               // Debug.Log("#### Site : " + site.name);
                yield return null;
            }
            announce();
        }
    }
}