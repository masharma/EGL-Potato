﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using EGL.DataModel;
using System.Linq;
using UnityEngine;
using EGL.Events;

namespace EGL.KMLParser
{
    public class ProductionJobsParser : KMLFolderParser<ProductionJob>,IEGLEventAnnouncer
    {
        List<ProductionJob> productionJobList;
        public ProductionJobsParser()
        {
            productionJobList = new List<ProductionJob>();
        }
        public void announce()
        {
            ParsingProductionJobCompletionEvent productionJobEvent = new ParsingProductionJobCompletionEvent(ref Han.currentSequence, this, productionJobList);
            EventHub.announceParsingProductionJobsCompletion(productionJobEvent);
        }

        public override IEnumerator ParseLogic()
        {
            XmlNodeList productionJobsNodeList = folder.SelectNodes(".//Placemark");
            foreach(XmlNode productionJobNode in productionJobsNodeList)
            {
                ProductionJob productionJob = new ProductionJob();
                
                string altitudeMode = productionJobNode["LineString"]["altitudeMode"].InnerText;
                productionJob.placemark.SetAltitudeMode(altitudeMode);
                
                String coordinatesAsString = productionJobNode["LineString"]["coordinates"].InnerText;
                char[] delimiterChars = { ',' };
                string[] coordinatesArray = coordinatesAsString.Split(delimiterChars);
                List<string> coordinatesStringList = new List<string>(coordinatesArray); 
                string coordinatesAsStringSubString = "";
                for (int i = 0; i < coordinatesArray.Length; i=i+3)
                {
                    var coordinatesSubArray = coordinatesStringList.GetRange(i, 3);
                    coordinatesAsStringSubString = string.Join(",", coordinatesSubArray.ToArray());
                    Coordinates coordinates = new Coordinates(coordinatesAsStringSubString);
                    productionJob.placemark.coordinatesList.Add(coordinates);
                }

                string beginDateTimeAsString = productionJobNode["TimeSpan"]["begin"].InnerText.Trim();
                beginDateTimeAsString = beginDateTimeAsString.Remove(beginDateTimeAsString.Length - 1);
                productionJob.begin = DateTime.Parse(beginDateTimeAsString);

                string endDateTimeAsString = productionJobNode["TimeSpan"]["end"].InnerText.Trim();
                endDateTimeAsString = endDateTimeAsString.Remove(endDateTimeAsString.Length - 1);
                productionJob.end = DateTime.Parse(endDateTimeAsString);
                productionJobList.Add(productionJob);
                if (productionJobList.Count > 600)
                    break;
                //Debug.Log("&&&& Production Job : " + productionJob.begin);
                yield return null;
            }
            announce();
        }
    }
}