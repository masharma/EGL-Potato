﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Collections;

namespace EGL.KMLParser
{
    public abstract class KMLFolderParser<T> : MonoBehaviour 
    {
        protected List<T> parsedContent = new List<T>();
        protected Sequence sequence;
        protected XmlNode folder { get; set; }
        public bool isParsing;
        public bool isParsingComplete;
        public KMLFolderParser() {
            isParsing = false;
            isParsingComplete = false;
            sequence = Han.currentSequence;
        }

        /// <summary>
        /// set isParsingComplete
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerator ParseLogic();
        public List<T> GetParsedContent()
        {
            return parsedContent;
        }
        public void Parse(XmlNode folder)
        {
            this.folder = folder;
            isParsing = true;
            isParsingComplete = false;
            StartCoroutine(ParseLogic());
        }   
    }
}
