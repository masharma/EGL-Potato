﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Xml;
using EGL.DataModel;
using System.Collections;
using EGL.Events;

namespace EGL.KMLParser {
    public class DataTransfersParser : KMLFolderParser<DataTransfer>, IEGLEventAnnouncer
    {
        List<DataTransfer> dataTransferList;
        public DataTransfersParser() 
        {
            dataTransferList = new List<DataTransfer>();
        }

        public void announce()
        {
            ParsingDataTransferCompletionEvent dataTransferEvent = new ParsingDataTransferCompletionEvent(ref Han.currentSequence, this, dataTransferList);
            EventHub.announceParsingDataTransferCompletion(dataTransferEvent);
        }

        public override IEnumerator ParseLogic()
        {
            //Debug.Log(folder.ToString());
            XmlNodeList dataTransfersNodeList = folder.SelectNodes(".//Placemark");
            foreach (XmlNode dataTransferNode in dataTransfersNodeList)
            {
                DataTransfer dataTransfer = new DataTransfer();
               
                string styleUrl = dataTransferNode["styleUrl"].InnerText;
                switch (styleUrl)
                {
                    case "#transferSuccess":
                        dataTransfer.status = true;
                        break;
                    case "#transferFailure":
                        dataTransfer.status = false;
                        break;
                }

                string altitudeMode = dataTransferNode["Point"]["altitudeMode"].InnerText;
                dataTransfer.placemark.SetAltitudeMode(altitudeMode);
                
                string coordinatesAsString = dataTransferNode["Point"]["coordinates"].InnerText;
                Coordinates coordinates = new Coordinates(coordinatesAsString);
               // dataTransfer.placemark.latitude = coordinates.latitude;
               // dataTransfer.placemark.longitude = coordinates.longitude;
              //  dataTransfer.placemark.altitude = coordinates.altitude;
                dataTransfer.placemark.coordinatesList.Add(coordinates);
                parsedContent.Add(dataTransfer);
                dataTransferList.Add(dataTransfer);
                //Debug.Log("$$$$ Data Transfer : " + dataTransfer.placemark.coordinatesList[0].latitude + " , " + dataTransfer.placemark.coordinatesList[0].longitude);
                if (dataTransferList.Count > 600)
                    break;
                yield return false;
            }
            isParsingComplete = true;
            Debug.Log("Parsing of Data Transfers Complete!!!");
            announce();
                 
        }
    }
}
