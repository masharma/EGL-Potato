﻿using EGL.DataModel;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Collections;
using UnityEngine;
using EGL.Events;

namespace EGL.KMLParser
{
    public class DataLinksParser : KMLFolderParser<DataLink>, IEGLEventAnnouncer 
    {
        public List<DataLink> dataLinkList;
        public DataLinksParser()
        {
            dataLinkList = new List<DataLink>();
        }

        public void announce()
        {
            ParsingDataLinkCompletionEvent dataLinkCompletionEvent = new ParsingDataLinkCompletionEvent(ref sequence, this, dataLinkList);
            EventHub.announceParsingDataLinkCompletion(dataLinkCompletionEvent);
        }

        public override IEnumerator ParseLogic()
        {
            XmlNodeList dataLinkNodeList = folder.SelectNodes(".//Placemark");
            foreach(XmlNode dataLinkNode in dataLinkNodeList)
            {
                DataLink dataLink = new DataLink();

                dataLink.name = dataLinkNode["name"].InnerText;
                string altitudeMode = dataLinkNode["LineString"]["altitudeMode"].InnerText;

                dataLink.placemark.SetAltitudeMode(altitudeMode);

                string allCoordinatesAsString = dataLinkNode["LineString"]["coordinates"].InnerText.Trim();
                char[] delimiterChars = { ' ' };
                string[] coordinatesStringArray = allCoordinatesAsString.Split(delimiterChars);
                foreach (string coordinatesAsString in coordinatesStringArray)
                {
                    Coordinates coordinates = new Coordinates(coordinatesAsString);
                    dataLink.placemark.coordinatesList.Add(coordinates);
                }

                string status = dataLinkNode["styleUrl"].InnerText;
                switch (status)
                {
                    case "#dataLinkOk":
                        dataLink.status = DataLinkStatus.OK;
                        break;
                    case "#dataLinkWarn":
                        dataLink.status = DataLinkStatus.WARN;
                        break;
                    case "#dataLinkCrit":
                        dataLink.status = DataLinkStatus.CRITICAL;
                        break;
                }
                dataLinkList.Add(dataLink);
                //Debug.Log("%%%% Data Link : " + dataLink.name);
                yield return null;
            }
           // Debug.Log("*************** Announce DataLinkParsing completed");
            announce();
            
        }
    }
}