﻿using UnityEngine;
using System.Collections;

namespace EGL.DataStructure
{
    public class Site
    {
        public string name { get; set; }
        public string description { get; set;}
        // long is 64-bit and signed
        public float latitude { get; set; }
        public float longitude { get; set; }
        public float altitude { get; set; }

        public Site(string name, string description, float latitude, float longitude, float altitude)
        {
            this.name = name;
            this.description = description;
            this.latitude = latitude;
            this.longitude = longitude;
            this.altitude = altitude;
        }
        
    }
}
