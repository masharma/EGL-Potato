﻿using UnityEngine;
using System.Collections;

public class RestClient : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //Debug.Log("pinging server");
        Get("http://cricapi.com/api/cricket/"); 
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void Get(string url)
    {
        UnityHTTP.Request getRequest = new UnityHTTP.Request("get", url);
        getRequest.Send((request) => {
            
            JSONObject data = new JSONObject(request.response.Text);
            //accessData(data);
            Debug.Log("Value is " + FindInJSONRecursively(data,"unique_id"));
        });
    }
    /// <summary>
    /// recursively logs the contents of a given <see cref="JSONObject"/> in the unity console. Use for Debugging resposne from endpoints
    /// </summary>
    /// <param name="obj">JSON Object used to print</param>
    void accessData(JSONObject obj)
    {
        switch (obj.type)
        {
            case JSONObject.Type.OBJECT:
                for (int i = 0; i < obj.list.Count; i++)
                {
                    string key = (string)obj.keys[i];
                    JSONObject j = (JSONObject)obj.list[i];
                    Debug.Log(key);
                    accessData(j);
                }
                break;
            case JSONObject.Type.ARRAY:
                foreach (JSONObject j in obj.list)
                {
                    accessData(j);
                }
                break;
            case JSONObject.Type.STRING:
                Debug.Log(obj.str);
                break;
            case JSONObject.Type.NUMBER:
                Debug.Log(obj.n);
                break;
            case JSONObject.Type.BOOL:
                Debug.Log(obj.b);
                break;
            case JSONObject.Type.NULL:
                Debug.Log("NULL");
                break;

        }
    }/// <summary>
    /// Method to return JSONObject, String, Number, Array values from JSONObject based on key
    /// </summary>
    /// <typeparam name="T">Type of the value to be returned. </typeparam>
    /// <param name="obj">the <see cref="JSONObject"/> to traverse</param>
    /// <param name="key">The key for the corresponding value</param>
    /// <returns>returns null if obj is not found, else returns the value corresponding to the key </returns>
    private JSONObject FindInJSON(JSONObject obj,string key)
    {
        for(int i = 0; i < obj.list.Count; i++)
        {
              if (obj.keys[i].Equals(key))
               {
                   return obj.list[i];
               }
        }
        return default(JSONObject);   
    }

    private JSONObject FindInJSONRecursively(JSONObject obj, string key)
    {
        
        for(int i = 0; i < obj.list.Count; i++)
        {
            if (obj.keys[i].Equals(key))
            {
                return obj.list[i];
            }else
            {
                accessData(obj.list[i]);
                FindInJSONRecursively(obj.list[i], key);
                
                
            }
        }
        return default(JSONObject);
    }
    
}
