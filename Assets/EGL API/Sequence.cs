﻿using EGL.DataModel;
using EGL.Events;
using EGL.KMLParser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace EGL
{
    public class Sequence : MonoBehaviour, IEGLEventListener 
    {
        public DateTime id;
        WineCellar wineCellar;
        public Sequence()
        {
            id = DateTime.Now;
            EventHub.registerParsingDataLinkCompletionListener(this);
            EventHub.registerParsingDataTransferCompletionListener(this);
            EventHub.registerParsingProductionJobCompletionListener(this);
            EventHub.registerParsingSiteCompletionListener(this);
            EventHub.registerSequenceCompletionListener(this);
            SequenceCompletionEventAnnouncer sequenceCompletionAnnouncer = new SequenceCompletionEventAnnouncer(this);
        }

        public void notify(IEGLEvent eglEvent)
        {
            //if (!(eglEvent is SequenceCompletedEvent) || !eglEvent.sequence.id.Equals(this.id))
            //    return;
            //// Now it is ensured that SequenceCompletedEvent was launched for this particular instance of Sequence
            //SequenceCompletedEvent sequenceCompletedEvent = (SequenceCompletedEvent)eglEvent;
            //Debug.Log("ACTUAL DATE IS " + this.id.ToShortDateString());
            //Debug.Log("DATE IN EVENT IS " + eglEvent.sequence.id.ToShortDateString());

            if (!eglEvent.sequence.id.Equals(this.id))
                return;
            
            if (eglEvent is ParsingDataLinkCompletionEvent)
            {
                ParsingDataLinkCompletionEvent parsingDataLinkCompletionEvent = (ParsingDataLinkCompletionEvent)eglEvent;
                int count = ((List<DataLink>)eglEvent.data).Count;
                Debug.Log("&&&&&&&& Sequence Id : " + this.id);
                Debug.Log("&&&&&&&& Parsing Data Link Complete. No of data links : " + count );
                
            }else if(eglEvent is ParsingDataTransferCompletionEvent)
            {
                ParsingDataTransferCompletionEvent parsingDataTransferCompletionEvent = (ParsingDataTransferCompletionEvent)eglEvent;
                int count = ((List<DataTransfer>)eglEvent.data).Count;
                Debug.Log("&&&&&&&& Sequence Id : " + this.id);
                Debug.Log("&&&&&&&& Parsing Data Transfers Complete. No of Data Transfers : " + count);
            }
            else if( eglEvent is ParsingProductionJobCompletionEvent)
            {
                ParsingProductionJobCompletionEvent parsingProductionJobCompletionEvent = (ParsingProductionJobCompletionEvent)eglEvent;
                int count = ((List<ProductionJob>)eglEvent.data).Count;
                Debug.Log("&&&&&&&& Sequence Id : " + this.id);
                Debug.Log("&&&&&&&& Parsing Production Job Complete. No of Production Jobs: " + count);
            }
            else if ( eglEvent is ParsingSiteCompletionEvent)
            {
                ParsingSiteCompletionEvent parsingSiteCompletionEvent = (ParsingSiteCompletionEvent)eglEvent;
                int count = ((List<Site>)eglEvent.data).Count;
                Debug.Log("&&&&&&&& Sequence Id : " + this.id);
                Debug.Log("&&&&&&&& Parsing Sites Complete. No of Sites : " + count);
            }
            else if(eglEvent is SequenceCompletionEvent)
            {
                SequenceCompletionEvent sequenceCompletionEvent = (SequenceCompletionEvent)eglEvent;
                Debug.Log("************************** Sequence ID : " + this.id + " **************************************");
                WineCellar wineCellar = (WineCellar)eglEvent.data;
                Debug.Log("No of Data Links : " + wineCellar.dataLinks.Count);
                Debug.Log("No of Data Transfers : " + wineCellar.dataTransfers.Count);
                Debug.Log("No of Production Jobs : " + wineCellar.productionJobs.Count);
                Debug.Log("No of Sites : " + wineCellar.sites.Count);
                Debug.Log("************************* Sequence successfully executed. Cleaning Up Listeners ******************");
                cleanUp();
            }else
            {
                Debug.Log("&&&&&& Unknown event fired for sequence? Source of error.");
            }
        }

        public void StartSequence()
        {
            Debug.Log("Staring Sequence");
            StartCoroutine(DownloadXML());
        }

        public void StartSequence(ref XmlDocument xmlDocument)
        {
            ParseXML(xmlDocument);
        }
        private IEnumerator DownloadXML() {
            Debug.Log("***** Creating XML object ******" + DateTime.Now.ToString());
            WWW www = new WWW("http://dashb-earth.cern.ch/dashboard/dashb-earth-all.kml");
            yield return www;
            
            if (www.error == null)
            {
                Debug.Log("***** XML Created *****" + DateTime.Now.ToString());
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(www.text);
                Debug.Log("XML fetched : " + www.text);
                ParseXML(xmlDocument);
            }
            else
            {
                Debug.Log("ERROR " + www.error);
                throw new Exception(www.error);
            }
        }
        private void ParseXML(XmlDocument xmlDocument)
        {
            GameObject parserObject = new GameObject();
            parserObject.AddComponent<DataTransfersParser>();
            parserObject.AddComponent<SitesParser>();
            parserObject.AddComponent<ProductionJobsParser>();
            parserObject.AddComponent<DataLinksParser>();

            SitesParser sitesParser = parserObject.GetComponent<SitesParser>();
            DataTransfersParser dataTransfersParser = parserObject.GetComponent<DataTransfersParser>();
            DataLinksParser dataLinksParser = parserObject.GetComponent<DataLinksParser>();
            ProductionJobsParser productionJobsParser = parserObject.GetComponent<ProductionJobsParser>();

            XmlNodeList folders = xmlDocument.SelectNodes("//kml/Document/Folder");
            for (int i = 0; i < folders.Count; i++)
            {
                XmlNode folder = folders[i];
                string folderName = folder["name"].InnerText;
                Debug.Log("Current Folder : " + folderName);
                switch (folderName)
                {
                    case "Sites":
                        sitesParser.Parse(folder);
                        break;
                    case "Data Transfers":
                        dataTransfersParser.Parse(folder);
                        break;
                    case "Data Links":
                        dataLinksParser.Parse(folder);
                        break;
                    case "Production Jobs":
                        productionJobsParser.Parse(folder);
                        break;
                }
                
                
            }
            
        }
        private void cleanUp()
        {
            EventHub.unregisterParsingDataLinkCompletionListener(this);
            EventHub.unregisterParsingDataTransferCompletionListener(this);
            EventHub.unregisterParsingProductionJobCompletionListener(this);
            EventHub.unregisterParsingSiteCompletionListener(this);
        }
        
    }
}
