﻿using System;
namespace EGL.DataModel
{
    public class ProductionJob
    {
        public DateTime begin { get; set; }
        public DateTime end { get; set; }
        public Placemark placemark { get; set; }
        public ProductionJob()
        {
            placemark = new Placemark();
        }

    }
}
