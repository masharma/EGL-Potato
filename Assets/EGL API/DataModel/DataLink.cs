﻿using System.Collections;
using System.Collections.Generic;
namespace EGL.DataModel
{
    public class DataLink
    {
        /// <summary>
        /// values : #dataLinkOk, #dataLinkWarn,#dataLinkCrit
        /// </summary>
        public DataLinkStatus status { get; set; }
        public string name { get; set; }
        public Placemark placemark { get; set; }
        public DataLink()
        {
            placemark = new Placemark();
        }

    }
    public enum DataLinkStatus
    {
        OK, WARN, CRITICAL 
    }
}