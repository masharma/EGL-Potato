﻿using System;
using System.Collections.Generic;

namespace EGL.DataModel
{
    /// <summary>
    /// Minimal implementation of Placemark defined by Google for Google Earth interface. Only fields used in the dashboard kml are included and all other defined in official specification are ignored. 
    /// This ensures a smaller memory footprint.
    /// <seealso cref="https://developers.google.com/kml/documentation/kmlreference#placemark"/>
    /// </summary>
    public class Placemark
    {
        public int id { get; set; }
        public string name { get; set; }
        public float altitude { get; set; }
        /// <summary>
        ///  can take values : 
        ///  1) realtiveToGround
        ///  2) clampToGround
        ///  3) absolute
        ///  4) relativeToSeaFloor
        ///  5) clampToSeaFloor
        ///  read more : https://developers.google.com/kml/documentation/kmlreference#point
        /// </summary>
        public AltitudeMode altitudeMode { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        /// <summary>
        /// Fields for Data Transfer
        /// </summary>
        /// 
        public List<Coordinates> coordinatesList;
        public Placemark()
        {
            coordinatesList = new List<Coordinates>();
        }
        public void SetAltitudeMode(string altitudeMode)
        {
            switch (altitudeMode)
            {
                case "relativeToGround":
                    this.altitudeMode = AltitudeMode.RELATIVE_TO_GROUND;
                    break;
                case "absolute":
                    this.altitudeMode = AltitudeMode.ABSOLUTE;
                    break;
                case "clampToGround":
                    this.altitudeMode = AltitudeMode.CLAMP_TO_GROUND;
                    break;
                case "relativeToSeaFloor":
                    this.altitudeMode = AltitudeMode.RELATIVE_TO_SEA_FLOOR;
                    break;
                case "clampToSeaFloor":
                    this.altitudeMode = AltitudeMode.CLAMP_TO_SEA_FLOOR;
                    break;
            }
        }

    }
    public enum AltitudeMode
    {
        RELATIVE_TO_GROUND, CLAMP_TO_GROUND, ABSOLUTE, CLAMP_TO_SEA_FLOOR, RELATIVE_TO_SEA_FLOOR
    }
}