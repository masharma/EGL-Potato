﻿
namespace EGL.DataModel
{
    public class Coordinates
    {
        public float latitude { get; set; }
        public float longitude { get; set; }
        public float altitude { get; set; }

        public Coordinates(float latitude, float longitude, float altitude)
        {
            this.latitude = latitude;
            this.longitude = longitude;
            this.altitude = altitude;
        }
        public Coordinates(string coordinatesAsString)
        {
            char[] delimiterChars = { ',' };
            string[] coordinateArray = coordinatesAsString.Split(delimiterChars);
            if (coordinateArray.Length > 0)
            {
                latitude = float.Parse(coordinateArray[0]);
                longitude = float.Parse(coordinateArray[1]);
                if (coordinateArray[2].Length > 0)
                {
                    altitude = float.Parse(coordinateArray[2]);
                }
            }else
            {
                throw new System.Exception("Cannot create coordinate from string : " + coordinatesAsString);
            }
        }

    }
}