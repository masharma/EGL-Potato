﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.DataModel
{
    public class Site
    {
        public string name { get; set; }
        public Placemark placemark { get; set; }
        public string description;
        public Site()
        {
            placemark = new Placemark();
        }
    }
}
