﻿using System;
namespace EGL.DataModel
{
    public class DataTransfer
    {

        /// <summary>
        /// #transferFailure or #transferSuccess
        /// </summary>
        public bool status { get; set; }

        public Placemark placemark { get; set; }
        public DateTime begin { get; set; }
        public DateTime end { get; set; }
        public DataTransfer()
        {
            placemark = new Placemark();
        }
    }
}
