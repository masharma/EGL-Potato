﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.DataModel
{
    public class WineCellar
    {
        public List<Site> sites { set; get; }
        public List<DataTransfer> dataTransfers { set; get; }
        public List<DataLink> dataLinks { set; get; }
        public List<ProductionJob> productionJobs { set; get; }
        public WineCellar(List<Site> sites,List<DataTransfer> dataTransfers, List<DataLink> dataLinks, List<ProductionJob> productionJobs)
        {
            this.dataLinks = dataLinks;
            this.dataTransfers = dataTransfers;
            this.productionJobs = productionJobs;
            this.sites = sites;
        }
    }
}
