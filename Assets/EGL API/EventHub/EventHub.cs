﻿using UnityEngine;
using System.Collections;
using EGL.Events;
using System;
using System.Collections.Generic;
/// <summary>
/// Warning : Do not remove ToArray() in statements like 
///  foreach (IEGLEventListener listener in newDataAavailableOnlineListeners.ToArray())
///  Because the list can be asyncronously accessed, it can get modified while the loop is running leading to InvalidOperationException : http://stackoverflow.com/questions/604831/collection-was-modified-enumeration-operation-may-not-execute
/// </summary>
public class EventHub{
    private static List<IEGLEventListener> parsingDataLinkCompletionListeners = new List<IEGLEventListener>();
    private static List<IEGLEventListener> parsingProductionJobCompletionListeners = new List<IEGLEventListener>();
    private static List<IEGLEventListener> parsingSiteCompletionListeners = new List<IEGLEventListener>();
    private static List<IEGLEventListener> parsingDataTransferCompletionListeners = new List<IEGLEventListener>();
    private static List<IEGLEventListener> newDataAavailableOnlineListeners = new List<IEGLEventListener>();
    private static List<IEGLEventListener> sequenceCompletionListeners = new List<IEGLEventListener>();
    internal static void announceParsingDataLinkCompletion(ParsingDataLinkCompletionEvent dataLinkCompletionEvent)
    {
        foreach (IEGLEventListener listener in parsingDataLinkCompletionListeners.ToArray())
        {
            listener.notify(dataLinkCompletionEvent);
        }
    }

    internal static void registerParsingDataLinkCompletionListener(IEGLEventListener listener)
    {
        parsingDataLinkCompletionListeners.Add(listener);
    }
    internal static void unregisterParsingDataLinkCompletionListener(IEGLEventListener listener)
    {
        if (parsingDataLinkCompletionListeners.Contains(listener))
        {
            parsingDataLinkCompletionListeners.Remove(listener);
        }
    }


    internal static void announceNewDataAvailableOnline(NewDataAvailableOnlineEvent newDataAvailableOnlineEvent)
    {
        foreach (IEGLEventListener listener in newDataAavailableOnlineListeners.ToArray())
        {
            listener.notify(newDataAvailableOnlineEvent);
        }
    }
    internal static void registerNewDataAvailableOnlineListener(IEGLEventListener listener)
    {
        newDataAavailableOnlineListeners.Add(listener);
    }
    internal static void unregisterNewDataAvailableOnlineListener(IEGLEventListener listener)
    {
        newDataAavailableOnlineListeners.Remove(listener);
    }

    internal static void announceParsingProductionJobsCompletion(ParsingProductionJobCompletionEvent parsingProductionJobCompletionEvent)
    {
        foreach (IEGLEventListener listener in parsingProductionJobCompletionListeners.ToArray())
        {
            listener.notify(parsingProductionJobCompletionEvent);
        }
    }
    internal static void registerParsingProductionJobCompletionListener(IEGLEventListener listener)
    {
        parsingProductionJobCompletionListeners.Add(listener);
    }
    internal static void unregisterParsingProductionJobCompletionListener(IEGLEventListener listener)
    {
        parsingProductionJobCompletionListeners.Remove(listener);
    }

    internal static void announceParsingDataTransferCompletion(ParsingDataTransferCompletionEvent parsingDataTransferCompletionEvent)
    {
        foreach (IEGLEventListener listener in parsingDataTransferCompletionListeners.ToArray())
        {
            listener.notify(parsingDataTransferCompletionEvent);
        }
    }
    internal static void registerParsingDataTransferCompletionListener(IEGLEventListener listener)
    {
        parsingDataTransferCompletionListeners.Add(listener);
    }
    internal static void unregisterParsingDataTransferCompletionListener(IEGLEventListener listener)
    {
        parsingDataTransferCompletionListeners.Remove(listener);
    }

    internal static void announceParsingSiteCompletion(ParsingSiteCompletionEvent parsingSiteCompletionEvent)
    {
        foreach (IEGLEventListener listener in parsingSiteCompletionListeners.ToArray())
        {
            listener.notify(parsingSiteCompletionEvent);
        }
    }
    internal static void registerParsingSiteCompletionListener(IEGLEventListener listener)
    {
        parsingSiteCompletionListeners.Add(listener);
    }
    internal static void unregisterParsingSiteCompletionListener(IEGLEventListener listener)
    {
        parsingSiteCompletionListeners.Remove(listener);
    }

    internal static void announceSequenceCompletion(SequenceCompletionEvent sequenceCompletionEvent)
    {
        foreach(IEGLEventListener listener in sequenceCompletionListeners.ToArray())
        {
            listener.notify(sequenceCompletionEvent);
        }
    }

    internal static void registerSequenceCompletionListener(IEGLEventListener listener)
    {
        sequenceCompletionListeners.Add(listener);
    }
    internal static void unregisterSequenceCompletionListener(IEGLEventListener listener)
    {
        sequenceCompletionListeners.Remove(listener);
    }
}
