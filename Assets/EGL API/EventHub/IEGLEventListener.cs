﻿using EGL.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events
{
    interface IEGLEventListener
    {
        void notify(IEGLEvent eglEvent );
    }
}
