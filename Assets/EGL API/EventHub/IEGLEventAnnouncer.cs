﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events
{
    public interface IEGLEventAnnouncer
    {
       void announce();
    }
}
