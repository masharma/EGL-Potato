﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace EGL.Events
{
    public class NewDataAvailableOnlineEvent : IEGLEvent
    {
        public NewDataAvailableOnlineEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy, XmlDocument xml):base(ref sequence,createdBy)
        {
            this.data = xml;
        }
    }
}
