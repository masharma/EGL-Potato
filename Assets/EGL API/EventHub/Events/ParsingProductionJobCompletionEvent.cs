﻿using EGL.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events 
{
    class ParsingProductionJobCompletionEvent : IEGLEvent
    {
        public ParsingProductionJobCompletionEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy,List<ProductionJob> productionJobs) : base(ref sequence, createdBy)
        {
            data = productionJobs;
        }
    }
}
