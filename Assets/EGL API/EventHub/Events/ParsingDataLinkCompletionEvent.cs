﻿using EGL.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events
{
    class ParsingDataLinkCompletionEvent : IEGLEvent
    {
        public ParsingDataLinkCompletionEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy, List<DataLink> datalinks) : base(ref sequence, createdBy)
        {
            data = datalinks;
        }
    }
}
