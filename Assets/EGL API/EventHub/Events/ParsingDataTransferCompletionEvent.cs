﻿using EGL.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events
{
    class ParsingDataTransferCompletionEvent : IEGLEvent
    {
        public ParsingDataTransferCompletionEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy, List<DataTransfer> dataTransfers) : base(ref sequence, createdBy)
        {
            data = dataTransfers;
        }
    }
}
