﻿using EGL.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events
{
    class SequenceCompletionEvent : IEGLEvent
    {
        public SequenceCompletionEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy, WineCellar wineCellar) : base(ref sequence, createdBy)
        {
            this.data = wineCellar;
        }

         
    }
}
