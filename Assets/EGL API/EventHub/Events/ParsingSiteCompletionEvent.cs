﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EGL.DataModel;
namespace EGL.Events
{
    class ParsingSiteCompletionEvent : IEGLEvent
    {
        public ParsingSiteCompletionEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy,List<Site> site):base(ref sequence, createdBy)
        {
            this.data = site;
        }
    }
}
