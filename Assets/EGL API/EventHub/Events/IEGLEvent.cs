﻿using UnityEngine;
using System.Collections;
using System;

namespace EGL.Events
{
    public abstract class IEGLEvent
    {
        public Sequence sequence;
        public DateTime createdOn;
        public IEGLEventAnnouncer createdBy;
        public System.Object data;
        public IEGLEvent(ref Sequence sequence, IEGLEventAnnouncer createdBy)
        {
            this.sequence = sequence;
            this.createdBy = createdBy;
            createdOn = DateTime.Now;
        }

    }
}