﻿using EGL.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL.Events
{
    class SequenceCompletionEventAnnouncer : IEGLEventAnnouncer,IEGLEventListener
    {
        private Sequence sequence;
        private bool dataLinkComplete;
        private bool siteComplete;
        private bool dataTransferComplete;
        private bool productionJobComplete;
        private List<Site> sites;
        private List<DataLink> dataLinks;
        private List<DataTransfer> dataTransfers;
        private List<ProductionJob> productionJobs;
        public SequenceCompletionEventAnnouncer(Sequence sequence)
        {
            this.sequence = sequence;
            EventHub.registerParsingDataLinkCompletionListener(this);
            EventHub.registerParsingDataTransferCompletionListener(this);
            EventHub.registerParsingProductionJobCompletionListener(this);
            EventHub.registerParsingSiteCompletionListener(this);
            dataLinkComplete = false;
            dataTransferComplete = false;
            productionJobComplete = false;
            siteComplete = false;
        }
        public void announce()
        {
            WineCellar wineCellar = new WineCellar(sites, dataTransfers, dataLinks, productionJobs);
            SequenceCompletionEvent sequenceCompletionEvent = new SequenceCompletionEvent(ref sequence, this, wineCellar);
            EventHub.announceSequenceCompletion(sequenceCompletionEvent);
            cleanUp();
        }

        public void notify(IEGLEvent eglEvent)
        {
            if (!eglEvent.sequence.id.Equals(sequence.id))
            {
                return;
            }
            if(eglEvent is ParsingDataLinkCompletionEvent)
            {
                dataLinkComplete = true;
                dataLinks = (List<DataLink>)eglEvent.data;
                checkForAnnouncement();
            }else if (eglEvent is ParsingDataTransferCompletionEvent)
            {
                dataTransferComplete = true;
                dataTransfers = (List<DataTransfer>)eglEvent.data;
                checkForAnnouncement();
            }else if( eglEvent is ParsingProductionJobCompletionEvent)
            {
                productionJobComplete = true;
                productionJobs = (List<ProductionJob>)eglEvent.data;
                checkForAnnouncement();
            }else if(eglEvent is ParsingSiteCompletionEvent)
            {
                siteComplete = true;
                sites = (List<Site>)eglEvent.data;
                checkForAnnouncement();
            }

        }
        private void checkForAnnouncement()
        {
            if (dataTransferComplete && dataLinkComplete && siteComplete && productionJobComplete)
                announce();
        }
        private void cleanUp()
        {
            EventHub.unregisterParsingDataLinkCompletionListener(this);
            EventHub.unregisterParsingDataTransferCompletionListener(this);
            EventHub.unregisterParsingProductionJobCompletionListener(this);
            EventHub.unregisterParsingSiteCompletionListener(this);
        }
    }
}
