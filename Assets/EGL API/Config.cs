﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGL
{
    public static class Config
    {
        public static float UPDATE_CHECK_PERIOD_IN_SECONDS = 30;

        public static double SERVER_REFRESH_SECONDS = 600;
    }
}
