﻿using EGL.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace EGL
{
    public class Han : MonoBehaviour,IEGLEventListener
    {
        public static Sequence currentSequence;
        private static Han instance;
        private GameObject sequenceObject;
        public static Han getInstance()
        {
            if (instance == null)
                instance = new Han();
            return instance;
        }
        private Han()
        {
            EventHub.registerNewDataAvailableOnlineListener(this);
        }
        static List<Sequence> sequenceQueue;

        public void notify(IEGLEvent eglEvent)
        {
            if (eglEvent is NewDataAvailableOnlineEvent)
            {
                XmlDocument xml = (XmlDocument)eglEvent.data;
                sequenceObject = null; // mark for garbage collection
                sequenceObject =  new GameObject();
                currentSequence = sequenceObject.AddComponent<Sequence>();
                Debug.Log("^^^^^^^^ Han says : Creating new sequence for the xml. Sequence ID : " + currentSequence.id);
                currentSequence.StartSequence(ref xml);
            }
        }
    }
}
