﻿using EGL.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace EGL.Service
{
    class ReconChewbacca : IEGLEventAnnouncer
    {
        XmlDocument xmlDocument;
        DateTime lastKMLExpiration;
        public void announce()
        {
            NewDataAvailableOnlineEvent newDataAvailableEvent = new NewDataAvailableOnlineEvent(ref Han.currentSequence, this, xmlDocument);
            EventHub.announceNewDataAvailableOnline(newDataAvailableEvent);
        }
        public IEnumerator huntForUpdates()
        {
            while (true)
            {
                Debug.Log("!!!! Recon Chewbacca : ***** Initiate fetch of XML ******" + DateTime.Now.ToString());
                WWW www = new WWW("http://dashb-earth.cern.ch/dashboard/dashb-earth-all.kml");
                while (!www.isDone)
                {
                    //Debug.Log("!!!! Recon Chewbacca : Waiting for fetch of xml to complete!");
                    yield return null;
                }
                
                if (www.error == null)
                {
                    Debug.Log("!!!! Recon Chewbacca : fetched XML at : " + DateTime.Now.ToString());
                    xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(www.text);
                    //Debug.Log("!!!! Recon Chewbacca : XML fetched looks like this : " + www.text);
                    string expires = xmlDocument.GetElementsByTagName("expires").Item(0).InnerText;
                    Debug.Log("!!!! ReconChewbacca : Fetched XML expires at : " + expires);
                    if (expires.Length > 0)
                    {
                        expires = expires.Remove(expires.Length - 1);
                    }
                    else
                    {
                        throw new Exception("!!!! Recon Chewcabba : Could not determine expire time for current sequence");
                    }
                    if (lastKMLExpiration == default(DateTime))
                    {
                        Debug.Log("!!!! Recon Chewbacca : Looks like you downloaded the kml for the first time");
                        lastKMLExpiration = DateTime.Parse(expires);
                        announce();
                    }
                    else
                    {
                        DateTime newKmlExpiration = DateTime.Parse(expires);
                        if (DateTime.Compare(newKmlExpiration, lastKMLExpiration) > 0)
                        {
                            Debug.Log("!!!! Recon Chewbacca : New data available online!!");
                            lastKMLExpiration = newKmlExpiration;
                            announce();
                        }else
                        {
                            Debug.Log("!!!! Recon Chewbacca : Nothing to update. No new data available from server");
                        }
                    }
                }
                else
                {
                    Debug.Log("!!!! Recon Chewbacca : ERROR while fetching KML from server : " + www.error);
                    Debug.Log("!!!! Recon Bhewbacca : Repeating fetch cycle in " + Config.UPDATE_CHECK_PERIOD_IN_SECONDS + " seconds");
                    //throw new Exception(www.error);
                    //Debug.Log("!!!! Recon Chewbacca : sleeping for " + Config.UPDATE_CHECK_PERIOD_IN_SECONDS);
                    //yield return new WaitForSeconds(Config.UPDATE_CHECK_PERIOD_IN_SECONDS);
                    
                }
                Debug.Log("!!!! Recon Chewbacca : sleeping for " + Config.UPDATE_CHECK_PERIOD_IN_SECONDS + " seconds");
                yield return new WaitForSeconds(Config.UPDATE_CHECK_PERIOD_IN_SECONDS);
            }
        }

    }
}
