﻿using UnityEngine;
using System.Collections;
using System.Xml;
using EGL.DataStructure;
using System.Collections.Generic;
using System;
using RestSharp;
using EGL.KMLParser;

public class XMLExplorer : MonoBehaviour  {
    public List<Site> sites = new List<Site>();
    public string test = "test failed";
    public string testFunction()
    {
        test = "test was modified.If you seee this.. test apssed";
        return test;
    }
    // Use this for initialization
    public bool isXMLFetched = false;
    public bool isSiteFolderProcessed = false;
	void Start () {
        //StartCoroutine(fetchXML());
        //StartCoroutine(CoroutineTest());
        // Get("http://dashb-earth.cern.ch/dashboard/dashb-earth-all.kml");
    }

    // Update is called once per frame
    void Update () {
        
	}

    IEnumerator fetchXML()
    {
        WWW www = new WWW("http://dashb-earth.cern.ch/dashboard/dashb-earth-all.kml");
        yield return www;
        if(www.error == null)
        {
            //Debug.Log("Loaded following XML" + www.text);
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(www.text);
            Debug.Log("XML fetched : " + www.text);
            Debug.Log("XML fetch complete..Sending it for processing.. ");
            StartCoroutine(processXML(xmlDocument));
            while (!isXMLFetched)
            {
                yield return null;
            }
            Debug.Log("Jumping out of getchXml()");
            //send for processing
        }
        else
        {
            Debug.Log("ERROR " + www.error);
        }
       
         
        
       

    }

    public void Get(string url)
    {
        UnityHTTP.Request getRequest = new UnityHTTP.Request("get", url);
        getRequest.Send((request) => {

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(request.response.Text);
            Debug.Log(request.response.Text);
            StartCoroutine(processXML(xmlDocument));
        });
    }
    public void GetViaRestSharp(string baseUrl,string relativeUrl)
    {
        Console.WriteLine("var client = new RestSharp.RestClient(baseUrl);");
        var client = new RestSharp.RestClient(baseUrl);
        Console.WriteLine("var request = new RestRequest(relativeUrl,Method.GET);");
        var request = new RestRequest(relativeUrl,Method.GET);
        // execute the request
        Console.WriteLine(" IRestResponse response = client.Execute(request);");
        IRestResponse response = client.Execute(request);
        Console.WriteLine("var content = response.Content");
        var content = response.Content; // raw content as string
        
        Debug.Log("LOADED XML");
        Debug.Log(content);

    }
    public IEnumerator GetViaCoRoutine(string url)
    {
        Debug.Log("*********** Sending Request ***********");
        UnityHTTP.Request request = new UnityHTTP.Request("get", url);
        Debug.Log("*********** Reqest object created***********");
        request.Send();
        Debug.Log("*********** Request Sent.... ***********");


        while (!request.isDone)
        {

            Debug.Log("*********** receiving in coroutine ***********");
            yield return null;
        }
        Debug.Log("*********** Request Complete ***********");
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(request.response.Text);
        Debug.Log(request.response.Text);
        Debug.Log("XML fetch complete..Sending it for processing.. ");
        StartCoroutine(processXML(xmlDocument));
        while (!isXMLFetched)
        {
            yield return null;
        }
        Debug.Log("Jumping out of GetViaCoroutine()");
        //Debug.Log("initializing www");
        //WWW www = new WWW(url);
        //while (!www.isDone)
        //{
        //    Debug.Log("receiving www data");
        //    yield return www;
        //}
        //Debug.Log("www done!!!");
        //Debug.Log(www.text);
    }

    public IEnumerator processXML(XmlDocument xmlDocument)
    {
        DataTransfersParser dataTransfersParser = new DataTransfersParser();
        yield return null;
        Debug.Log("Starting Line : " + dataTransfersParser);
        Debug.Log("Processing XML");
        XmlNodeList folders = xmlDocument.SelectNodes("//kml/Document/Folder");
        //Debug.Log("Count : " + folders.Count);
        for (int i = 0; i < folders.Count; i++)
        {
            XmlNode folder = folders[i];
            //Debug.Log(folder.SelectSingleNode("//name").InnerText);
            string folderName = folder["name"].InnerText;
            Debug.Log("Current Folder : " + folderName);

            switch (folderName)
            {
                case "Sites":// StartCoroutine(processSites(folder));
                    break;
                case "Data Transfers" :
                    Debug.Log(folder.Value);
                    if(dataTransfersParser == null)
                    {
                        Debug.Log("**********************************************************************Data Transfer Parser is null");
                       // dataTransfersParser = new DataTransfersParser(folder);
                    }
                    //StartCoroutine(dataTransfersParser.Parse(folder));
                    //StartCoroutine(PopulateDataTransfers(folder));
                    break;  
            }
            //Debug.Log("Name" + site.SelectSingleNode("name").InnerText + site.SelectSingleNode("//Placemark/Name").InnerText);
            yield return null;
        }
        while(!isSiteFolderProcessed)
        {
            yield return false;
        }
        while (!dataTransfersParser.isParsingComplete)
        {
            Debug.Log("Parsing Data Transfers");
            yield return false;
        }

        isXMLFetched = true;
        Debug.Log("XML processing completed!");
       
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sitesFolder"></param>
    private IEnumerator processSites(XmlNode sitesFolder)
    {
        Debug.Log("Processing Sites folder.....");
//        List<Site> sites = new List<Site>();
        XmlNodeList placemarks = sitesFolder.SelectNodes(".//Placemark");
        Debug.Log("No of placemarks : " + placemarks.Count );
        Debug.Log("Populating Site with the following information");
        foreach (XmlNode placemark in placemarks)
        {
            string name = placemark["name"].InnerText;
            //Debug.Log(placemark["LookAt"]["latitude"].InnerText);
            float latitude = Convert.ToSingle(placemark["LookAt"]["latitude"].InnerText);
            float longitude = Convert.ToSingle(placemark["LookAt"]["longitude"].InnerText);
            float altitude = float.Parse(placemark["LookAt"]["altitude"].InnerText);
            string description = placemark["description"].InnerText;
           // Debug.Log(name + " : " + description + " : " + longitude + " : " + latitude + " : " + altitude);
            Site site = new Site(name,description,latitude,longitude,altitude);
            sites.Add(site);
            yield return null;
        }
        
        isSiteFolderProcessed = true;
    }

    public IEnumerator fetchSites()
    {
        isXMLFetched = false;
        isSiteFolderProcessed = false;
        //Console.WriteLine("GetViaRestSharp");
        //Debug.Log("GetViaRestSharp");
        //GetViaRestSharp("http://www.w3schools.com/xsl/books.xml", "/"); 
        //yield return null;
        //StartCoroutine(GetViaCoRoutine("http://dashb-earth.cern.ch/dashboard/dashb-earth-all.kml"));
        StartCoroutine(fetchXML());
        while (!this.isXMLFetched)
        {
            Debug.Log("waiting....");
            yield return null;
        }
        Debug.Log("Verifying if site data has been loaded correctly");
        foreach (Site site in this.sites)
        {
            Debug.Log(site.name + " : " + site.latitude);
            yield return null;
        }
        Debug.Log("Site data available now!!! ");

    }

    public IEnumerator PopulateDataTransfers(XmlNode dataTransfersFolder)
    {
        Debug.Log("Processing Data Transfers");
        yield return null;
    }
    public IEnumerator CoroutineTest()
    {
        Debug.Log("coroutine test");
        yield return null;
        Debug.Log("after yield");
        for (int i = 0; i < 10; i++)
        {
            yield return null;
            Debug.Log("i incremented " + i);
            //yield return null;
            Debug.Log("baad waala");
        }
        StartCoroutine(SubCoroutineTest());
        Debug.Log("this should be last statement");
        
    }
    public IEnumerator SubCoroutineTest()
    {
        for(int i = 0; i < 10; i++)
        {
            Debug.Log("SubCoroutine. If executed after the 'this should be last statement, means subcoroutines are called in a separate thread and a bool in a loop is required after each subcoroutine call to check status'");
            yield return null;
        }
    }
    
}
